(function($){

    var list = $('.definition_list');

    list.find('dd').hide();

    list.find('dt').on('click', function (){
       $(this).addClass('toggle').next()
                     .slideToggle()
                     .siblings('dd').removeClass('toggle').slideUp();
    });

})(jQuery);

(function($){
    var users = $('.list-group').children();

    users.on('click', function (){
        if (window.location.href.charAt(window.location.href.length-1) !== 'e'){
            document.getElementById('eval_form').action = window.location.href.
            replace(window.location.href.charAt(window.location.href.length-1), '/') +  $(this).attr('id');
        }else{
            document.getElementById('eval_form').action = window.location.href + "/" + $(this).attr('id');
        }

        document.getElementById('ID').value = $(this).attr('id');
        document.getElementById('ID_logged').value = '{{Auth::id()}}';

        var name = $(this).find('h5').text().split(' ');
        document.getElementById('FirstName').value = name[0];
        document.getElementById('LastName').value = name[1];

    });
})(jQuery);

(function ($){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });

    var elements = $('.editable');
    elements.dblclick(function (){
        elements.each(function (){
            $(this).prop('readonly', true);
        })
        $(this).attr('readonly', false);
    })

    elements.change(function (){
        var data = {
            id:  $(this).parent('div').children('input[name="id"]').val(),
            field: $(this).attr('name'),
            newValue: $(this).val(),
        };

        var request = $.ajax({
            url: '/ajax',
            type: 'POST',
            data: data,
        });
        request.done(function (msg){
            console.log(msg);
            if(data['field'] === 'password'){
                $(this).val(msg['password']);
            }
        });
    });

    $('button.delete').click(function (e) {
        e.preventDefault();
        var data = {id: $(this).parent('div').children('input[name="id"]').val()};
        console.log(data['id']);
        var request = $.ajax({
            url: '/ajax/drop',
            type: 'POST',
            data: data,
        });
        request.done(function (msg){
            if(msg === 'done')
                $('#user-' + data['id']).remove();
        });
    })
})(jQuery)

// (function ($) {
//     var form = $('#profile_form');
//
//     form.on('submit', function (event) {
//         event.preventDefault();  //zastavi defaultne spravanie eventu
//
//         var request = $.ajax({
//             url: form.attr('action'),
//             type: form.attr('method'),
//             data: form.serialize()
//         });
//
//         request.done(function (data){
//             console.log(data);
//         });
//     })
// })(jQuery);
