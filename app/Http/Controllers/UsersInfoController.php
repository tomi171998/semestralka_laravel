<?php

namespace App\Http\Controllers;

use App\Models\UserInfo;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use \Validator;

class UsersInfoController extends Controller
{

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = User::all();
        return view('logged.evaluate')->with('users', $users);
    }

    /**
     * Display a listing of the resource.
     */
    public function getOne($id)
    {
        $user = User::find($id);
        return view('logged.evaluate')->with([
            'user_eval'=> $user,
            'users' => $this->getUsers()
        ]);
    }

    public function getForEvaluation(){

        return view('logged.evaluate')->with([
            'users_info' => $this->getUsers()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'FirstName'             => 'required',
            'LastName'              => 'required',
            'Email'                 => 'required|email',
            'password'              => 'required|min:8',
            'password_confirmation' => 'required|min:8',
            'Street'                => 'required',
            'City'                  => 'required',
            'PostalCode'            => 'required|numeric',
            'Country'               => 'exists:states',
            'Company'               => 'required',
        ], [], [
            'FirstName'             => 'First name',
            'LastName'              => 'Last name',
            'Email'                 => 'Email',
            'password'              => 'Password',
            'password_confirmation' => 'Repeat Password',
            'Street'                => 'Street',
            'City'                  => 'City',
            'PostalCode'            => 'Postal code',
            'Country'               => 'Country',
            'Company'               => 'Company',
        ]);

        if ($validator->fails()){
            return back()->withErrors($validator->errors())->withInput();
        }

        $user = new User();
        $user->first_name = $request->input('FirstName');
        $user->last_name = $request->input('LastName');
        $user->email = $request->input('Email');
        $user->password = Hash::make($request->input('password'));
        $user->street = $request->input('Street');
        $user->city = $request->input('City');
        $user->postal_code = $request->input('PostalCode');
        $user->country = $request->input('Country');
        $user->company = $request->input('Company');

        $user->save();
        return redirect('registration');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->first_name = $request->input('inputFName');
        $user->last_name = $request->input('inputLName');
        $user->email = $request->input('inputEmail');
        $user->password = $request->input('inputPassword1');
        $user->street = $request->input('inputStreet');
        $user->city = $request->input('inputCity');
        $user->postal_code = $request->input('inputPostalCode');
        $user->country = $request->input('inputCountry');
        $user->company = $request->input('inputCompany');

        $user->save();
        $index = $this->index();
        return redirect('delete')->with(['users'=> $index]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $user = User::destroy($id);
        return redirect('delete');
    }

    private function getUsers(){
        return UserInfo::where('id_user', '!=', Auth::id())->get();
    }
}
