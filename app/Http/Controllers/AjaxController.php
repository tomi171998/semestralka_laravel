<?php

namespace App\Http\Controllers;

use App\Models\Evaluation;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class AjaxController extends Controller
{
    public function update(Request $request){
        $data = $request->all();
        $user = User::find($data['id']);

        switch ($data['field']){
            case 'first_name':
                try {
                    $this->validate($request, ['newValue' => 'required|string|max:255']);
                }catch(\Throwable $e){
                    return 'Something went wrong!';
                }
                $user->userInfo->first_name = $data['newValue'];
                break;
            case 'last_name':
                try {
                    $this->validate($request, ['newValue' => 'required|string|max:255']);
                }catch(\Throwable $e){
                    return 'Something went wrong!';
                }
                $user->userInfo->last_name = $data['newValue'];
                break;
            case 'email':
                try {
                    $this->validate($request, ['newValue' => 'required|string|email|max:255']);
                }catch(\Throwable $e){
                    return 'Something went wrong!';
                }
                $user->email = $data['newValue'];
                break;
            case 'password':
                $user->password = Hash::make($data['newValue']);
                break;
        }
        if($user->isDirty()){  //check if fields was changed
            $user->save();
            $msg = 'User updated';
        }
        if($user->userInfo->isDirty()){  //check if fields was changed
            $user->userInfo->save();
            $msg = 'User information updated';
        }
        return ['msg'=>$msg, 'password'=>$user->password];
    }

    public function drop(Request $request){
        $data = $request->all();
        $user = User::find($data['id']);

        if(isset($user)){
            foreach($user->evaluationsWho as $eval){
                Evaluation::where('id', '=', $eval->id)->delete();
            }
            UserInfo::where('id', '=', $user->userInfo->id)->delete();
            $user->delete();
            $msg = 'done';
        }
        return $msg;
    }

    public function updateProfile(Request $request){

    }
}
