<?php

namespace App\Http\Controllers;

use App\Models\State;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use Auth;
use Validator;

class ProfileController extends Controller
{

    public function getLogged(){
        $user = User::find(Auth::id());
        $states = State::all();

        return view('logged.profile')->with([
            'user' => $user,
            'states' => $states
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $validator =
        $validator = Validator::make($request->all(), [
            'firstName'             => 'required',
            'lastName'              => 'required',
            'email'                 => 'required|email',
            'street'                => 'required',
            'city'                  => 'required',
            'postalCode'            => 'required|numeric',
            'country'               => 'exists:states',
            'company'               => 'required',
        ]);

        if ($validator->fails()){
            return back()->withErrors($validator->errors())->withInput();
        }

        $user = User::find(Auth::id());
        $user->email = $request->email;

        if($user->isDirty()){  //check if fields was changed
            $user->save();
        }

        $userInfo = $user->userInfo;
        $userInfo->first_name = $request->firstName;
        $userInfo->last_name = $request->lastName;
        $userInfo->company = $request->company;

        if($userInfo->isDirty()){  //check if fields was changed
            $userInfo->save();
        }

        $address = $user->userAddr;
        $address->street = $request->street;
        $address->city = $request->city;
        $address->postal_code = $request->postalCode;
        $address->country = $request->country;

        if($address->isDirty()){  //check if fields was changed
            $address->save();
        }

        return redirect()->route('profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
