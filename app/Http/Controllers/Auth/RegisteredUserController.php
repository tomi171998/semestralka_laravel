<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\UserAddress;
use App\Models\User;
use App\Models\UserInfo;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\State;
use Illuminate\Support\Facades\Log;
use Validator;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create()
    {
        $states = State::all();
        return view('nologged.registration')->with(['states' => $states]);
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstName'             => 'required|string|max:255',
            'lastName'              => 'required|string|max:255',
            'email'                 => 'required|string|email|max:255|unique:users',
            'password'              => 'required|string|confirmed|min:8',
            'street'                => 'required',
            'city'                  => 'required',
            'postalCode'            => 'required|numeric',
            'country'               => 'required|exists:states',
            'company'               => 'required',
        ], [], [
            'firstName'             => 'First name',
            'lastName'              => 'Last name',
            'email'                 => 'Email',
            'password'              => 'Password',
            'password_confirmation' => 'Repeat Password',
            'street'                => 'Street',
            'city'                  => 'City',
            'postalCode'            => 'Postal code',
            'country'               => 'Country',
            'company'               => 'Company',
        ]);

        if ($validator->fails()){
            return back()->withErrors($validator->errors())->withInput();
        }

        $user = User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $address = UserAddress::firstOrCreate(
            [
                'street' => $request->street,
                'city' => $request->city,
                'postal_code' => $request->postalCode,
                'country' => $request->country,
            ],
            [
                'street' => $request->street,
                'city' => $request->city,
                'postal_code' => $request->postalCode,
                'country' => $request->country,
            ]
        );

        $user_info = UserInfo::create([
            'first_name' => $request->firstName,
            'last_name' => $request->lastName,
            'company' => $request->company,
            'id_user' => $user->id,
            'id_address' => $address->id,
        ]);

        return redirect('/')->with('info', 'Successfully registered.');
    }
}
