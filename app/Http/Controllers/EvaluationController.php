<?php

namespace App\Http\Controllers;

use App\Models\Evaluation;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Validator;

class EvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = Evaluation::all();
        return view('logged.evaluate')->with('users', $users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'evaluation' => 'required',
            'description' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator->errors())->withInput();
        }

//        $user_who_evaluate = User::find(Auth::id());
//        $user_evaluated = User::find($request->ID);

        $evaluation = new Evaluation();
        $evaluation->evaluation = $request->evaluation;
        $evaluation->description = $request->description;
        $evaluation->id_who_evaluate = Auth::id();
        $evaluation->id_evaluated = $request->ID;
//        $evaluation->user()->associate($user_evaluated);
//        $evaluation->userWho()->associate($user_who_evaluate);
        $evaluation->save();

        return redirect('/evaluate');
    }

    public function getMyEvaluations()
    {

        $evaluations = Evaluation::with(['user.userInfo'])->where('id_evaluated', '=', Auth::id())->get();
        return view('logged.myevaluations')->with(['evaluations' => $evaluations]);
    }
}
