<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    use HasFactory;

    public $table = 'users_address';

    protected $fillable = ['street', 'city', 'postal_code', 'country'];
}
