<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    use HasFactory;

    public $table = 'evaluations';

    protected $fillable = ['id_who_evaluate', 'id_evaluated', 'evaluation', 'description'];

    public function userWho(){
        return $this->belongsTo(User::class, 'id_who_evaluate');
    }

    public function user(){
        return $this->belongsTo(User::class, 'id_evaluated');
    }

    public function delete(){
        $this->delete();
    }
}
