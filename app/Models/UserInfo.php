<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    use HasFactory;

    public $table = 'users_info';

    protected $fillable = ['first_name', 'last_name', 'company','id_user','id_address'];

    public function user(){
        return $this->belongsTo(User::class, 'id_user');
    }

}
