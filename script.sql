INSERT INTO evaluation.states (id, Country) VALUES (1, 'Slovakia');
INSERT INTO evaluation.states (id, Country) VALUES (2, 'Czech Republic');
INSERT INTO evaluation.states (id, Country) VALUES (3, 'Hungary');
INSERT INTO evaluation.states (id, Country) VALUES (4, 'Poland');
INSERT INTO evaluation.states (id, Country) VALUES (5, 'Ukraine');
INSERT INTO evaluation.states (id, Country) VALUES (6, 'Austria');

INSERT INTO evaluation.users ( email, email_verified_at, password, remember_token, is_admin, created_at, updated_at) VALUES ( 'admin@admin.sk', null, '$2y$10$S0hUeFZJUldOsrP4CS2BFe/e67wd30uLdHQ51b9dCBIVwsZ.XNxUa', null, 1, null, null);
