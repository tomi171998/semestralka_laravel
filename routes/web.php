<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StatesCotroller;
use App\Http\Controllers\UsersInfoController;
use App\Http\Controllers\EvaluationController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AjaxController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('profile', [ProfileController::class, 'getLogged'])
    ->middleware('auth')
    ->name('profile');

Route::post('profile', [ProfileController::class, 'update'])
    ->middleware('auth')
    ->name('profile.update');

Route::get('home', function () {
    return view('logged.home');
})->middleware('auth')->name('logged.home');

Route::get('admin', [AdminController::class, 'index'])
    ->middleware('auth')
    ->name('admin');

Route::get('myevaluations', [EvaluationController::class, 'getMyEvaluations'])
    ->middleware('auth')
    ->name('logged.myevaluations');

Route::get('evaluate', [UsersInfoController::class, 'getForEvaluation'])
    ->middleware('auth')
    ->name('logged.evaluate.get');

Route::post('evaluate/{id}', [EvaluationController::class, 'store'])
    ->middleware('auth')
    ->name('logged.evaluate.post');

Route::post('/ajax', [AjaxController::class, 'update'])
    ->middleware('auth');

Route::post('/ajax/drop', [AjaxController::class, 'drop'])
    ->middleware('auth');

Route::post('/ajax/update', [AjaxController::class, 'updateProfile'])
    ->middleware('auth');

require __DIR__.'/auth.php';
