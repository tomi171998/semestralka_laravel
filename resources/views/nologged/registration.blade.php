@extends('nologged.master')

@section('content')
<div class="reg shadow-lg mb-5 bg-white rounded container">
    <div class="row">
        <div class="nav_img col-xl">
            <h1>Information</h1>
            <p>
                Chupa chups pudding candy tiramisu ice cream. Biscuit croissant liquorice icing tiramisu chocolate cake tiramisu caramels donut. Cookie cheesecake fruitcake lemon drops jujubes danish jelly beans. Pie chocolate croissant dragée cake tart jujubes candy muffin.<br>
            </p>
            <p>
                Pie bear claw biscuit dragée halvah pie gingerbread wafer. Sweet roll candy sugar plum liquorice jujubes cupcake powder soufflé pudding. Tart bear claw macaroon. Wafer gingerbread cheesecake cookie sweet roll.
            </p>
            <form action="{{route('login')}}">
                <button type="submit" class="btn_style btn btn-primary" >Have an account</button>
            </form>
        </div>
        <div class="formular col-xl">
            <h1>Register</h1>
            <form method="post" action="{{route('register')}}">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input type="text" class="form-control @error('firstName') is-invalid @enderror" id="firstName" name="firstName" placeholder="First name"  value="{{old('firstName')}}">

                        @error('firstName')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <input type="text" class="form-control @error('lastName') is-invalid @enderror" id="lastName" name="lastName" placeholder="Last name" value="{{old('lastName')}}">

                        @error('lastName')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Email Address" value="{{old('email')}}">

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="text" class="form-control @error('street') is-invalid @enderror" id="street" name="street" placeholder="Street" value="{{old('street')}}">

                    @error('street')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input type="text" class="form-control @error('city') is-invalid @enderror" id="city" name="city" placeholder="City" value="{{old('city')}}">

                        @error('city')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-4">
                        <select id="country" name="country" class="form-control @error('country') is-invalid @enderror">
                            <option selected>Choose...</option>
                            @foreach($states as $state)
                                <option>{{$state->Country}}</option>
                            @endforeach
                        </select>

                        @error('country')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-2">
                        <input type="text" class="form-control @error('postalCode') is-invalid @enderror" id="postalCode" name="postalCode" placeholder="Post Code" value="{{old('postalCode')}}">

                        @error('postalCode')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control @error('company') is-invalid @enderror" id="company" name="company" placeholder="Company name" value="{{old('company')}}">

                    @error('company')
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input type="password" class="form-control custom-control-label @error('password') is-invalid @enderror" id="password" name="password" placeholder="Password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6">
                        <input type="password" class="form-control custom-control-label @error('password_confirmation') is-invalid @enderror" id="password_confirmation" name="password_confirmation" placeholder="Repeat password">

                        @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn_style btn btn-primary">Sign in</button>
            </form>
        </div>
    </div>
</div>
@stop
