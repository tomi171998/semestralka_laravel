@extends('nologged.master')

@section('content')
<div class="reg shadow-lg mb-5 bg-white rounded container">
    <div class="row">
        <div class="nav_img col-xl">
            <h1>Information</h1>
            <p>
                Chupa chups pudding candy tiramisu ice cream. Biscuit croissant liquorice icing tiramisu chocolate cake tiramisu caramels donut. Cookie cheesecake fruitcake lemon drops jujubes danish jelly beans. Pie chocolate croissant dragée cake tart jujubes candy muffin.<br>
            </p>
            <p>
                Pie bear claw biscuit dragée halvah pie gingerbread wafer. Sweet roll candy sugar plum liquorice jujubes cupcake powder soufflé pudding. Tart bear claw macaroon. Wafer gingerbread cheesecake cookie sweet roll.
            </p>
            <form action="{{route('login')}}">
                <button type="submit" class="btn_style btn btn-primary">Sign in</button>
            </form>
        </div>
        <div class="formular col-xl">
            <h1>Reset password</h1>
            <form method="post" action="{{ route('password.update') }}">
                @csrf
                <div class="form-group">
                    <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="name@company.com" value="{{ old('email') }}">

                    @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Password">

                    @error('password')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <input name="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" id="password_confirmation" placeholder="Repeat password">

                    @error('password_confirmation')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn_style btn btn-primary">Reset</button>
            </form>
        </div>
    </div>
</div>
@stop
