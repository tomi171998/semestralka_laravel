@extends('nologged.master')

@section('content')
<div class="index_bg">
    <img src="{{ asset('Img/ciernyObdlznik.png') }}" alt="asdf">
</div>
<div class="index_form">
    <h1>Log in</h1>

    @if(session('info'))
        <div class="alert alert-info" role="alert">
            {{session('info')}}
        </div>
    @endif

    <form class="main_form form-control-lg" method="post" action="{{ route('login') }}">
        @csrf
        <div class="form-group">
            <label class="text-dark font-weight-bold">Email address</label>
            <input name="email" class="form-control @error('email') is-invalid @enderror" type="email" id="email_login" placeholder="name@company.com" value="{{ old('email') }}">

            @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label class="text-dark font-weight-bold">Password</label>
            <input name="password" class="form-control @error('password') is-invalid @enderror" type="password" id="password_login" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;&bull;">

            @error('password')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <p>Don't have an account? <a href="{{ route('register') }}" class="text-decoration-none">Sign up</a></p>
        <button type="submit" class="btn btn-primary btn-lg">Submit</button>
        <a href="{{ route('password.reset') }}" class="text-muted text-decoration-none">Forgot password?</a>
    </form>
</div>
@stop
