@extends('admin.master')

@section('content')

    <h1>Admin section</h1>

    <div class="edit">
        <div class="row">
            <div class="head id">Id</div>
            <div class="head name">First name</div>
            <div class="head name">Last name</div>
            <div class="head email">Email</div>
            <div class="head password">Password</div>
        </div>

        <input type="hidden" id="_token" value="{{ csrf_token() }}">
        @foreach($users as $user)
            <div class="row" id="user-{{$user->id}}">
                <input class="editable id" name="id" value="{{$user->id}}" disabled>
                <input class="editable name" name="first_name" value="{{$user->userInfo->first_name}}" readonly>
                <input class="editable name" name="last_name" value="{{$user->userInfo->last_name}}" readonly>
                <input class="editable email" name="email" value="{{$user->email}}" readonly>
                <input class="editable password" name="password" value="{{$user->password}}" readonly>
                <button type="button" class="btn btn-danger delete">Delete</button>
            </div>
        @endforeach
    </div>

@endsection
