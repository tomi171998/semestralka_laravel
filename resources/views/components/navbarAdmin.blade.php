
<div class="navbar_transparent container">
    <nav class="navbar navbar-expand-lg">
        <span class="logged_user navbar-text">{{Auth::user()->email}}</span>
        <a class="navbar-brand" href="{{route('logout')}}" >
            <img alt="Log out" src="{{asset('Img//logout_icon.png')}}">
        </a>
    </nav>
</div>
