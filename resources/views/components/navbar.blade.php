{{--@extends('logged.master')--}}

{{--@section('navbar')--}}
<div class="navbar_transparent container">
    <nav class="navbar navbar-expand-lg">
        <span class="logged_user navbar-text">{{Auth::user()->email}}</span>
        <a class="navbar-brand" href="{{route('logged.home')}}" >
            <img alt="Home" src="{{asset('Img/home_icon.png')}}">
        </a>
        <a class="navbar-brand" href="{{route('profile')}}" >
            <img alt="Profile" src="{{asset('Img/profile_icon.png')}}">
        </a>
        <a class="navbar-brand" href="{{route('logout')}}" >
            <img alt="Log out" src="{{asset('Img//logout_icon.png')}}">
        </a>
    </nav>
</div>
{{--@endsection--}}
