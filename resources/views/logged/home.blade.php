@extends('logged.master')

@section('content')
    <div class="selection_screen shadow-lg mb-5 bg-white rounded container">
        <div class="vr"></div>
        <div class="row">
            <div class="my_eval col-xl">
                <h1>My evaluations</h1>
                <p>In this section you are able to see how other teammates evaluate you.</p>
                <form method="get">
                    <button type="submit" class="btn_style btn btn-primary" formaction="{{route('logged.myevaluations')}}">Go to my evaluations</button>
                </form>
            </div>
            <div class="eval col-xl">
                <h1>Evaluate</h1>
                <p>In this section you are able to evaluate your teammate and send them your evaluation.</p>
                <form>
                    <button type="submit" formaction="{{route('logged.evaluate.get')}}" class="btn_style btn btn-primary">Go evaluate others</button>
                </form>
            </div>
        </div>
    </div>
@endsection
