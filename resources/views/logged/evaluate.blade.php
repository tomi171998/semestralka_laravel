@extends('logged.master')

@section('content')
    <div class="selection_screen shadow-lg mb-5 bg-white rounded container">
        <div class="vr"></div>
        <div class="row">
            <div class="select_eval col-xl">
                <h1>Select for evaluation</h1>
                <div class="list-group">
                    @foreach($users_info as $user_info)
                        <a id="{{$user_info->id_user}}" class="list-group-item list-group-item-action">
                            <div>
                                <h5>{{$user_info->first_name.' '.$user_info->last_name}}</h5>
                                <small>{{$user_info->user->email}}</small>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
            <div class="selected_eval col-xl">
                <form method="post" id="eval_form">
                    @csrf
                    <div class="form-row">
                        <input name="ID" id="ID" hidden>
                        <input name="ID_logged" id="ID_logged" hidden>

                        <div class="form-group col-md-6">
                            <label for="FirstName" class="text-light font-weight-bold">First Name</label>
                            <input type="text" class="form-control" id="FirstName" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="LastName">Last Name</label>
                            <input type="text" class="form-control" id="LastName" disabled>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Evaluation">Evaluation</label>
                            <select name="evaluation" id="Evaluation" class="form-control @error('evaluation') is-invalid @enderror">
                                <option disabled selected>Choose</option>
                                <option>Excellent</option>
                                <option>Very good</option>
                                <option>Satisfactory</option>
                                <option>Unsatisfactory</option>
                            </select>
                            @error('evaluation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md">
                            <label for="Description">Description</label>
                            <textarea name="description" rows="2" class="form-control @error('description') is-invalid @enderror" id="Description"></textarea>
                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn_style btn btn-primary">Save evaluation</button>
                </form>
            </div>
        </div>
    </div>
@endsection
