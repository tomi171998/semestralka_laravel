@extends('logged.master')

@section('content')
    <div class="evaluation_list">
        <div class="container">
            <dl class="definition_list">
                @foreach($evaluations as $eval)
                    <dt><a href="#">Evaluation from: {{$eval->userWho->userInfo->first_name.' '.$eval->userWho->userInfo->last_name}}</a></dt>
                    <dd><small>Evaluation: <br> {{$eval->evaluation}}</small><br>Description: <br>{{$eval->description}}</dd>
                @endforeach
            </dl>

        </div>
    </div>
@endsection
